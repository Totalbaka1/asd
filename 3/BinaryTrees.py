#!/usr/bin/env python
# coding: utf-8

# In[3]:


from binarytree import Node


# In[40]:


#бинарный поиск ближайшего меньшего для insert
def binarysearch(array, findelement, arraysize):
    left = 0
    right = len(array)
    middle = int((left + right)/2)
    index = -1
    while (left < right-1):
        if array[middle] > findelement:
            right = middle
        else:
            left = middle
        middle = int((left + right)/2)
   
    index = middle
    return index


# In[41]:


#Вставка нового элемента в бинарное дерево
#т.е. идет проверка вхождения элемента, а затем если этот элемент не нашелся в месте, где должен находиться - 
#добавляется в место где он должен находиться
def insert(root, key):
    parent = None
    node = root
    #проход по дереву в поисках элемента
    while node != None and key != node.value:
        if node.value < key:#перемещение в правое поддерево
            leftright = True#если true, то добавляем справа от родителя
            parent = node
            node = node.right
        else:#перемещение в левое поддерево
            leftright = False#если false, то добавляем слева от родителя
            parent = node
            node = node.left
    #если элемент не найден - добавляем
    if node == None:
        if leftright :
            parent.right = Node(key)
        else:
            parent.left = Node(key)
        return True
    elif node.value == key:#если найден - заканчиваем
        return False
    return root


# In[43]:


# самое базовое дерево без балансировок
# в каждой ноде доступ только к значению + правому и левому соседям, из-за этого не пройтись по родителям
# потому будем использовать отсортированный список для поиска ближайшего большего
# считали количество точек для массива
pointscount = int(input())

current = 0
listofnumbers = []#список для хранения элементов (и поиска ближайшего большего)

#в начале дерево пустое, добавляем элемент в корневую ноду
if pointscount > 0:
    a = int(input())
    root = Node(a)
    listofnumbers.append(a)
    print('- -')
    current += 1


#добавляем остальные элементы
while current < pointscount:
    # добавляем элемент 
    a = int(input())
    newelement = insert(root, a)
    if(newelement):#если элемент новый
        if listofnumbers[len(listofnumbers)-1] < a:#если элемент максимальный
            listofnumbers.append(a)
        else:
            ind = binarysearch(listofnumbers, a, len(listofnumbers))#иначе ищем ближайшего большего
            if(listofnumbers[ind] < a):#если бинарный поиск выдал ближайшего меньшего
                ind+=1
            listofnumbers.insert(ind, a)#добавляем
    indexl = listofnumbers.index(a)
    if newelement :#если элемента не было
        if indexl + 1 < len(listofnumbers):
            print('-',listofnumbers[indexl + 1])
        else:
            print('- -')
    else:#если элемент был
        if indexl + 1 < len(listofnumbers):
            print('+',listofnumbers[indexl + 1])
        else:
            print('+ -')
    current+=1
    


# In[ ]:





# In[ ]:




